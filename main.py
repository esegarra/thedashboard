from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys, subprocess
import PrimaryLayout
import math, urllib.request, numpy as np
import sqlite3, os, pandas as pd
from datetime import datetime,  date, timedelta
import time
import configparser
from DateAxis import DateAxis
import pyqtgraph as pg
from time_series_functions import *

class myTableModel(QAbstractTableModel):
	def __init__(self, datain, headerdata, parent=None, *args):
		QAbstractTableModel.__init__(self, parent, *args) 
		self.arraydata = datain
		self.headerdata = headerdata

	def rowCount(self, parent): 
		return self.arraydata.shape[0]

	def columnCount(self, parent): 
		return self.arraydata.shape[1]

	def data(self, index, role): 
		if not index.isValid(): 
			return QVariant() 
		elif role != Qt.DisplayRole: 
			return QVariant()

		# Handling different column data types
		if self.headerdata[index.column()] == 'Year':
			if math.isnan(self.arraydata.iloc[index.row()][index.column()]): # Handling null values
				return QVariant()
			return QVariant(str(int(self.arraydata.iloc[index.row()][index.column()])))  #QVariant(self.arraydata[index.row()][index.column()])
		elif self.headerdata[index.column()] == 'DateRead':
			if self.arraydata.iloc[index.row()][index.column()] == date(1000, 1, 1): # Handling null values (this is the null date)
				return QVariant()
			return QVariant(str(self.arraydata.iloc[index.row()][index.column()])) 
		elif self.headerdata[index.column()] == 'ID':
			if math.isnan(self.arraydata.iloc[index.row()][index.column()]): # Handling null values
				return QVariant()
			return QVariant(int(self.arraydata.iloc[index.row()][index.column()]))  #QVariant(self.arraydata[index.row()][index.column()])
		else:
			return QVariant(str(self.arraydata.iloc[index.row()][index.column()]))  #QVariant(self.arraydata[index.row()][index.column()]) 

	def headerData(self, col, orientation, role):
		if orientation == Qt.Horizontal and role == Qt.DisplayRole:
			return QVariant(self.headerdata[col])
		return QVariant()

class theDashboardApp(QtWidgets.QMainWindow, PrimaryLayout.Ui_MainWindow):
	## Aux parameters ##
	null_date = date(1000, 1, 1)
	null_date_int = 10000101
	indexTitles = ["Mind", "Body", "Soul"]
	index_Goals = [4, 10, 10] # For the [Mind, Body, Soul] indexes
	index_Horizons = [21.0, 7.0, 7.0] # Horizons over which to calculate each index (in days)
	today_int = int(str(date.today()).replace('-','')) # Todays date as a int (YYYYMMDD)
	# col head = [['ID', 'Author1', 'Author2', 'Year', 'Title', 'DateRead', 'DateCreated', 'DateModifiedF', 'Path', 'MendDateAdd', 'MendDateMod', 'MendRead', 'Projects']]
	visible_cols = [False,  True, 		False, 	True, 	True, 		True, 		False, False, False, False, False, False, False]
	h_scale = 40   #height of one row in the table
	w_scale = 200  # length for Author1 (for scaling column widths)
	col_widths = [0.3,  	1, 			1, 		0.3, 		4,	   0.6, 			0.6, 		0.6, 		3, 		0.6, 		0.6, 			0.4, 2] # widths in terms of w_scales
	search_col = -1  #defaults to searching all fields

	int_to_date = lambda x: date(int(str(x)[0:4]), int(str(x)[4:6]), int(str(x)[6:8]))

	def __init__(self):
		super(theDashboardApp, self).__init__()
		self.setupUi(self)

		## Implmenting settings in config file
		self.initConfig()

		## Grabbing current article DB and constructing indices and colors
		self.alldocs = self.get_article_db()
		self.currdocs = self.alldocs
		self.update_Index_Colors()

		#print(self.alldocs[115:122])
		#print(self.alldocs.iloc[0:10][['ID', 'Author1', 'Year', 'DateRead']])

		# Attaching plot to graphics view
		axis = DateAxis(orientation='bottom')
		self.dataplot = self.graphicsView_DataPlot.addPlot(title="", axisItems = {'bottom': axis})		
		#self.dataplot = pg.PlotWidget(axisItems={'bottom': axis}, enableMenu=False, title="What What?!")
		self.DrawPlot()
		
		# Attaching functions to widgets in main tab
		self.pushButtonPickTasks.clicked.connect(self.pickNewTasks)
		self.pushButtonLogTasks.clicked.connect(self.logTasks)

		# Attaching functions to widgets in article viewer tab
		self.pushButton_Drawboard.clicked.connect(self.OpenDrawBoard)
		self.pushButton_Mendeley.clicked.connect(self.OpenMendeley)
		self.pushButton_ReadToday.clicked.connect(lambda: self.MarkReadOrUnreadDialog("MarkRead", True))
		self.pushButton_ReadCustom.clicked.connect(lambda: self.MarkReadOrUnreadDialog("MarkRead", False))
		self.pushButton_Unread.clicked.connect(lambda: self.MarkReadOrUnreadDialog("MarkUnread", False))
		# Putting up documents in Table View
		self.header = self.alldocs.columns
		self.tm = myTableModel(self.alldocs, self.header , self)
		#self.tableView_ArticleTable.setModel(tm)
		
		# This in-between model will allow for sorting and easier filtering
		self.proxyModel = QtCore.QSortFilterProxyModel(self)
		self.proxyModel.setSourceModel(self.tm)
		self.tableView_ArticleTable.setModel(self.proxyModel)
		self.tableView_ArticleTable.verticalHeader().setDefaultSectionSize(self.h_scale)
		self.tableView_ArticleTable.setSelectionBehavior(QAbstractItemView.SelectRows)  # Makes whole row selected instead of single cells
		self.InitColumnsCheckBoxes()
		self.InitSearchFeatures()
		self.InitFilterComboBoxes()

		# Attaching functions to widgets in exercise tab
		self.comboBox_PlotVar.currentIndexChanged.connect(self.DrawPlot)
		self.comboBox_PlotTrans.currentIndexChanged.connect(self.DrawPlot)
		self.comboBox_PlotHorizon.currentIndexChanged.connect(self.DrawPlot)
		self.comboBox_Activity.currentIndexChanged.connect(self.activityChanged)
		self.pushButton_EnterWeight.clicked.connect(self.enterWeight)
		self.pushButton_EnterActivity.clicked.connect(self.enterActivity)
		# Setting dates to today
		self.dateEdit_WeightDate.setDateTime(QtCore.QDateTime.currentDateTime())
		self.dateEdit_ActivityDate.setDateTime(QtCore.QDateTime.currentDateTime())

		#print(self.alldocs.columns)
		#print(self.alldocs.head())
		#print(self.alldocs.iloc[2][4])


	############################### Auxilary Methods (Main Tab) ##############################
	def update_Index_Colors(self):
		indexNumbers = self.calc_indices()
		box_list = [self.frame_MindBox, self.frame_BodyBox, self.frame_SoulBox]
		label_list = [self.label_MindNumber, self.label_BodyNumber, self.label_SoulNumber]
		for i in range(3):
			label_list[i].setText(str(indexNumbers[i]))
			temp_box = box_list[i]
			p = temp_box.palette()
			# Index is defined such that its red for [0, 0.5*goal] and chanes from red to 
			# 	green on [0.5*goal, goal], and is green ever after
			index_num = 2*indexNumbers[i]/self.index_Goals[i] - 1 
			if index_num == 0: index_num = 0
			p.setColor(temp_box.backgroundRole(), QtGui.QColor(self.double_to_color(index_num))) #"#FF3800"))
			temp_box.setPalette(p)
			temp_box.setAutoFillBackground(True)

		self.label_diagnostic.setText('Calculating the indices to be {}, {}, and {}. Updating colors'.format(indexNumbers[0], indexNumbers[1], indexNumbers[2]))

	def double_to_color(self, index):
		# index should be a double between 0 and 1
		# 0 = red, and 1 = green, this function returns the
		# hex color closest to the number specified
		# This site produces gradients: http://www.perbang.dk/rgbgradient/
		if index < 1/10:    return "#FF0000"
		elif index < 2/10:      return "#FF3800"
		elif index < 3/10:      return "#FF3800"
		elif index < 4/10:      return "#FF7100"
		elif index < 5/10:      return "#FFAA00"
		elif index < 6/10:      return "#FFE200"
		elif index < 7/10:      return "#E2FF00"
		elif index < 8/10:      return "#A9FF00"
		elif index < 9/10:      return "#71FF00"
		elif index < 10/10:     return "#38FF00"
		else:                   return "#00FF00"
	
	def calc_indices(self):
		# Calculating Mind index (avg # of articles read in past horizon)
		mask = (self.alldocs['DateRead'] != self.null_date) & (self.alldocs['DateRead'] > date.today() - timedelta(days=self.index_Horizons[0]))
		#(alldocs['DateRead'] == "") & (alldocs['DateModifiedF'] < date.today() - timedelta(days=183))
		articles_read = round(len(self.alldocs[mask])/(self.index_Horizons[0]/7),1)

		# Calculating Body and Soul indices (avg # of km run and minutes meditated in past horizon)
		tempConn = sqlite3.connect(self.aux_db_path) # "ElanDB.sqlite")
		tempC = tempConn.cursor()
		tempC.execute("SELECT distance, activityDate, duration FROM Activities where activity = '{}'".format("Running"))
		self.runDB = pd.DataFrame(tempC.fetchall(), columns=["Distance", "Date", "Duration"])
		tempC.execute("SELECT duration, activityDate FROM Activities where activity = '{}'".format("Meditating"))
		meditDB = pd.DataFrame(tempC.fetchall(), columns=["Duration", "Date"])

		int_to_date = lambda x: date(int(str(x)[0:4]), int(str(x)[4:6]), int(str(x)[6:8]))
		int_to_time = lambda x: time.mktime(time.strptime(str(x), "%Y%m%d") )
		self.runDB['ActivityDate'] = self.runDB['Date'].apply(int_to_date)
		self.runDB['ActivityDateTime'] = self.runDB['Date'].apply(int_to_time)
		self.runDB.sort_values(by=['Date'], inplace= True)
		meditDB['ActivityDate'] = meditDB['Date'].apply(int_to_date)

		mask = (self.runDB['ActivityDate'] > date.today() - timedelta(days=self.index_Horizons[1]))
		tempRunDB = self.runDB[mask]
		km_run = round(sum(tempRunDB['Distance']/(self.index_Horizons[1]/7)),1) # Need to find a roundin function

		mask = (meditDB['ActivityDate'] > date.today() - timedelta(days=self.index_Horizons[2]))
		meditDB = meditDB[mask]
		min_meditated = round(sum(meditDB['Duration'])/self.index_Horizons[2],1)

		tempConn.close()
		return [articles_read, km_run, min_meditated]
	
	def pickNewTasks(self):
		self.label_diagnostic.setText('Pick tasks button was pressed. Still need to implement this feature.')

	def logTasks(self):
		self.label_diagnostic.setText('Log tasks button was pressed. Still need to implement this feature.')
		self.update_Index_Colors()

	############################### Auxilary Methods (Article Tab) ##############################
	def InitColumnsCheckBoxes(self):
		# Need to establish defaults (which are checked on initially)
		self.ColCheckList = [self.checkBox_ID, self.checkBox_Author1, self.checkBox_Author2, self.checkBox_Year, self.checkBox_Title,
							self.checkBox_DateRead, self.checkBox_DateCreatedF, self.checkBox_DateModifiedF, self.checkBox_FilePath, 
							self.checkBox_DateAddedM, self.checkBox_DateModifiedM, self.checkBox_ReadM, self.checkBox_Projects]

		# Scaling the column widths
		self.col_widths = [w*self.w_scale for w in self.col_widths]
		#print(self.col_widths)

		# Hides columns according to default visible columns and sets their initial widths
		for col in range(len(self.visible_cols)):
			self.tableView_ArticleTable.setColumnWidth(col, self.col_widths[col])
			if not self.visible_cols[col]:
				self.tableView_ArticleTable.hideColumn(col)

		# Check boxes for visible columns and attach toggle event
		for i in range(len(self.ColCheckList)):
			if self.visible_cols[i]: self.ColCheckList[i].setChecked(True)
			self.ColCheckList[i].stateChanged.connect(self.AdjustVisibleColumns)

	def AdjustVisibleColumns(self):
		# First change our bool list to match the check boxes (this captures the change wherever it is)
		for i in range(len(self.visible_cols)):
			if (self.visible_cols[i] != self.ColCheckList[i].isChecked()):
				self.visible_cols[i] = self.ColCheckList[i].isChecked()
				#print(self.visible_cols)

		# Second we use visible_cols to show/hide columns (only adjusts columns that don't match visible_cols)
		for i in range(len(self.visible_cols)):
			if self.visible_cols[i] == self.tableView_ArticleTable.isColumnHidden(i): # Checks if (should be visible) is same as (currently hidden) (which means need to switch)
				if self.visible_cols[i]: self.tableView_ArticleTable.showColumn(i)
				elif not self.visible_cols[i]: self.tableView_ArticleTable.hideColumn(i)
				else: print("Something is wrong with the visible_cols object, it contains non-boolean values")
	
	def InitFilterComboBoxes(self):
		# conn = sqlite3.connect(self.mend_db_path) #"ElanDB.sqlite")
		# curs = conn.cursor()
		# curs.execute("SELECT id , name, parentId FROM Folders")
		# folders = pd.DataFrame(curs.fetchall(), columns=['Folder_ID', 'Name', 'Parent_ID'])

		# Dividing up parent and child folders
		par_folds = self.folders[self.folders['Parent_ID']==-1].sort_values(by=['Name'])
		chi_folds = self.folders[self.folders['Parent_ID']!=-1].sort_values(by=['Name'])

		# Sequentially Adding the parent folders and child folders underneath
		self.comboBox_Project_Choices = ['All projects']   # First choice (and default)
		for p in range(par_folds.shape[0]):
			par_ID = par_folds.iloc[p]['Folder_ID']
			self.comboBox_Project_Choices += [par_folds.iloc[p]['Name']]

			for f in range(chi_folds.shape[0]):
				if chi_folds.iloc[f]['Parent_ID'] == par_ID:
					self.comboBox_Project_Choices += ['   {}'.format(chi_folds.iloc[f]['Name'])]
		
		# Adding the projects to the combo box
		self.comboBox_Project.addItems(self.comboBox_Project_Choices)
		
		# Connecting combo box to action
		self.comboBox_Project.currentIndexChanged.connect(self.FilterEngaged)
		#print(self.folders)

	def FilterEngaged(self):
		curr_choice = self.comboBox_Project.currentText().lstrip()
		if curr_choice == 'All projects':
			self.proxyModel.setFilterRegExp('')
		else:
			self.lineEdit_SearchField.setText('')  # Setting the search field text to empty
			self.search_col = 12
			self.proxyModel.setFilterRegExp(curr_choice)

		self.proxyModel.setFilterKeyColumn(self.search_col)
		self.label_diagnostic.setText('Now listing only documents from {}.'.format(curr_choice))
		#self.SearchEngaged()

	def InitSearchFeatures(self):
		self.tableView_ArticleTable.setSortingEnabled(True)				# Enable column sorting
		self.proxyModel.setFilterCaseSensitivity (False)				# Enable regex to be case insensitive
		
		self.lineEdit_SearchField.returnPressed.connect(self.SearchEngaged) # Connect search field to the search filter
		#self.lineEdit_SearchField.textChanged.connect(self.SearchEngaged) # Alternative, but takes too long

		# Hooking up seach field combo box
		self.proxyModel.setFilterKeyColumn(self.search_col)  # Sets search field to the defaul defined above
		self.comboBox_searchColumns.currentIndexChanged.connect(self.SearchEngaged)

	def SearchEngaged(self):
		self.comboBox_Project.setCurrentIndex(0)  # Setting project combo box to 'all projects'
		self.ChangeSearchFields()
		self.proxyModel.setFilterRegExp(str(self.lineEdit_SearchField.text()))

	def ChangeSearchFields(self):
		newChoice = self.comboBox_searchColumns.currentText()
		if newChoice == 'all columns': 	self.search_col = -1
		elif newChoice == 'authors':	self.search_col = 1
		elif newChoice == 'year':		self.search_col = 3
		elif newChoice == 'title':		self.search_col = 4
		else:
			self.label_diagnostic('Problem setting new search field, combo box item not recognized')
			return
		self.proxyModel.setFilterKeyColumn(self.search_col)
		self.label_diagnostic.setText('Now searching on '+self.comboBox_searchColumns.currentText())
		#self.SearchEngaged()

	def ValidateDateText(self, date_text):
		# This methods verifies that the string sent has the form YYYY-MM-DD
		try:
			if date_text != datetime.strptime(date_text, "%Y-%m-%d").strftime('%Y-%m-%d'):
				raise ValueError
			return True
		except ValueError:
			return False

	def OpenDrawBoard(self):
		command = 'start drawboardpdf:'
		sel_rows = self.tableView_ArticleTable.selectionModel().selectedRows()
		if sel_rows == []:		# action if no rows are selected
			os.system(command)
			self.label_diagnostic.setText('Opening drawboard application.')
		else:					# action if there are rows selected
			sel_row_indices = [i.row() for i in sorted(sel_rows)]
			first_author1 = self.proxyModel.index(sel_row_indices[0],1).data()
			first_year = self.proxyModel.index(sel_row_indices[0],3).data()
			first_filepath = self.proxyModel.index(sel_row_indices[0],8).data().replace('/','\\')
			#print(command+' '+first_filepath)
			os.system(command)   # Need to use os.system for apps
			self.label_diagnostic.setText('Opening drawboard application (still need to figure a way to send filepath of {} ({}) to drawboard).'.format(first_author1, str(first_year)))

	def OpenMendeley(self):
		command = 'C:\Programs\Mendeley Desktop\MendeleyDesktop.exe'
		sel_rows = self.tableView_ArticleTable.selectionModel().selectedRows()
		if sel_rows == []:		# action if no rows are selected
			subprocess.Popen([command])
			self.label_diagnostic.setText('Opening Mendeley application.')
		else:
			sel_row_indices = [i.row() for i in sorted(sel_rows)]
			first_author1 = self.proxyModel.index(sel_row_indices[0],1).data()
			first_year = self.proxyModel.index(sel_row_indices[0],3).data()
			first_filepath = self.proxyModel.index(sel_row_indices[0],8).data().replace('/','\\')
			#print(command+' '+first_filepath)
			subprocess.Popen([command, first_filepath])
			self.label_diagnostic.setText('Opening {} ({}) document in Mendeley.'.format(first_author1, str(first_year)))

	def MarkReadOrUnreadDialog(self, read_unread, today_bool):
		sel_rows = self.tableView_ArticleTable.selectionModel().selectedRows()
		sel_row_indices = [i.row() for i in sorted(sel_rows)]
		sel_author1s = [self.proxyModel.index(i,1).data() for i in sel_row_indices]
		sel_years = [self.proxyModel.index(i,3).data() for i in sel_row_indices]
		sel_titles = [self.proxyModel.index(i,4).data() for i in sel_row_indices]
		sel_read = [self.proxyModel.index(i,5).data() for i in sel_row_indices]
		self.label_diagnostic.setText('Selected indices'+str(sel_row_indices)+', and titles '+str(sel_titles))
		#self.label_diagnostic.setText('Mark read button was pressed. Still need to implement this feature.')

		# Dialog box confirming that the selected rows are to be marked as read (or unread)
		if read_unread == "MarkRead":	
			text = "Documents to be marked as read: <br>"
		else:	
			text = "Documents to be marked as unread: <br>"
		none_read = True
		all_read = True
		for i in range(len(sel_author1s)):
			a = sel_author1s[i].split(',')[0] # Just grabbing the last name
			y = sel_years[i]
			t = sel_titles[i]
			r = sel_read[i]
			text = text + "<br>   {} - {} - {}...".format(a, str(y), t[0:35])
			if r is not None:  
				text = text + " (read on {})".format(r)
				none_read = False
			else:
				all_read = False

		text = text + "<br> <br>"
		if (not none_read and read_unread == "MarkRead"): 
			text = text + "Some documents are already marked as read. Their read dates will be overwritten. "
		
		if (read_unread == "MarkUnread"):
			if not all_read: # Exit because trying to mark unread as unread
				self.label_diagnostic.setText("Some of selected document(s) are already unread.")
				return
			text = text + "The following document's read dates will be erased. "
			text = text + "Are you sure about marking these {} document(s) as unread?".format(len(sel_titles))
			confirmMsg = QMessageBox()
			confirmMsg.setWindowTitle('Confirm Marking Unread')
			confirmMsg.setText(text)
			confirmMsg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
			reply = confirmMsg.exec_()

			if reply == QMessageBox.Yes:
				# Go through each and mark as unread
				for i in range(len(sel_years)):
					self.MarkReadOrUnread(sel_row_indices[i], False, 0)
				# Then rebuild the table view to show changes
				self.alldocs = self.get_article_db()
				self.tm.arraydata = self.alldocs
				self.label_diagnostic.setText('Marking the document(s) as unread.')
				self.update_Index_Colors()
				return
			else:
				self.label_diagnostic.setText('Leaving document(s) as is.')
				return

		if today_bool:  	# Indictes marking read as of today
			text = text + "Are you sure about marking these {} document(s) as read as of today ({})?".format(len(sel_titles), str(date.today()))
			confirmMsg = QMessageBox()
			confirmMsg.setWindowTitle('Confirm Marking Read')
			confirmMsg.setText(text)
			confirmMsg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
			reply = confirmMsg.exec_()
			if reply == QMessageBox.Yes: reply = True   # Converting so both dialogs yield boolean for acceptance
			read_Date = str(date.today())
		else: 				# Indicates that the mark date will be chosen
			text = text + " Choose a date (YYYY-MM-DD) to have these {} document(s) marked as read.".format(len(sel_titles))
			read_Date, reply = QInputDialog.getText(self, 'Choose Date and Confirm Marking Read', text)

		# Validating date entry (should be of the form YYYY-MM-DD)
		if not self.ValidateDateText(read_Date):
			self.label_diagnostic.setText('Date is not in the correct format (YYYY-MM-DD). Leaving document(s) as is.')
			return

		read_Date = str(read_Date).replace('-','')
		#print(read_Date)

		if reply == True:
			self.label_diagnostic.setText('Marking the document(s) as read as of {}.'.format(read_Date))
			# Go through each and mark as unread (if read already) then mark as read.
			for i in range(len(sel_years)):
				if sel_read[i] is not None:  
					self.MarkReadOrUnread(sel_row_indices[i], False, read_Date)
				self.MarkReadOrUnread(sel_row_indices[i], True, read_Date)
			# Then rebuild the table view to show changes
			self.alldocs = self.get_article_db()
			self.tm.arraydata = self.alldocs
			self.update_Index_Colors()
		elif reply == False:
			self.label_diagnostic.setText('Leaving document(s) as is.')
		else:
			self.label_diagnostic.setText('Dialog box result was not recognized, something is wrong...')

	def MarkReadOrUnread(self, rowNum, mark_Read_Bool, dateReadText):
		# This method marks a document as read (adds a row to ElanDB) or unread (removes the row from ElanDB)
		# The boolean mark_read_bool specifies which action is to be taken (mark_read_bool = True implies add row)
		# Note that 'rowNum' should be the row in the current view (which is not necessarily the same row as in alldocs)
		if mark_Read_Bool:
			# Grabbing the document's information
			docID = self.proxyModel.index(rowNum,0).data()
			author1 = self.proxyModel.index(rowNum,1).data()
			year = self.proxyModel.index(rowNum,3).data()
			title = self.proxyModel.index(rowNum,4).data()
			filepath = self.proxyModel.index(rowNum,8).data()
			
			# Opening Connection to database and inserting row
			conn = sqlite3.connect(self.aux_db_path) # "ElanDB.sqlite")
			c = conn.cursor()

			com = 'INSERT INTO ArticlesRead (Doc_ID, DateRead, Author, Year, Title, File_Path, DateEntered) VALUES'
			val_list = '({},{},"{}",{},"{}","{}",{})'.format(docID, dateReadText, author1, year, title, filepath, self.today_int)
			print(com + val_list)
			c.execute(com + val_list)

			# Save (commit) the changes and close connection
			conn.commit()
			conn.close()
			return
		else:
			# Grabbing the document's information
			docID = self.proxyModel.index(rowNum,0).data()

			# Opening Connection to database and inserting row
			conn = sqlite3.connect(self.aux_db_path) #"ElanDB.sqlite")
			c = conn.cursor()

			com = "DELETE FROM ArticlesRead WHERE Doc_ID = {}".format(docID)
			#print(com)
			c.execute(com)

			# Save (commit) the changes and close connection
			conn.commit()
			conn.close()
			return

	############################### Auxilary Methods (Exercise Tab) ##############################
	def activityChanged(self):
		# Adjusts the line edit fields to match the current activity selected
		# Getting the current activity
		activityType = str(self.comboBox_Activity.currentText())

		if activityType == 'Running':
			print('Updating fields for running entry.')
			self.lineEdit_ActivityChar2.setEnabled(True)
			self.label_ActivityChar2.setText('Distance (km):')
		elif activityType == 'Push Ups':
			print('Updating fields for push ups entry.')
			self.lineEdit_ActivityChar2.setEnabled(True)
			self.label_ActivityChar2.setText('Number:')
		elif activityType == 'Sit Ups':
			print('Updating fields for sit ups entry.')
			self.lineEdit_ActivityChar2.setEnabled(True)
			self.label_ActivityChar2.setText('Number:')
		elif activityType == 'Meditating':
			print('Updating fields for meditating entry.')
			self.lineEdit_ActivityChar2.setEnabled(False)
			self.label_ActivityChar2.setText('N/A:')
		else:
			print('Combo box Activity not recognized: {}'.format(activityType))

	def enterWeight(self):
		# Extracting information from the fields
		weightDate = str(self.dateEdit_WeightDate.date().toPyDate()).replace('-','')
		weight = self.lineEdit_Weight.text()
		
		# Opening Connection to database and inserting row
		conn = sqlite3.connect(self.aux_db_path) #"ElanDB.sqlite")
		c = conn.cursor()

		com = "INSERT INTO Weight (weight, weightDate, entryDate) VALUES"
		val_list = "({},{},{})".format(weight, weightDate, self.today_int)
		print('Issuing the following sql command: {}'.format(com + val_list))
		c.execute(com + val_list)

		# Save (commit) the changes and close connection
		conn.commit()
		conn.close()

		# Updating indices and colors.
		self.update_Index_Colors()

		self.label_diagnostic.setText('Entered {} as weight measured on {}'.format(weight, weightDate))

	def enterActivity(self):
		### Currently this assumes entry of a runnin number, need to adjust for other types ###
		# Extracting information from the fields
		activityType = str(self.comboBox_Activity.currentText())
		activityDate = str(self.dateEdit_ActivityDate.date().toPyDate()).replace('-','')

		# Changing behavior based on activity
		if activityType == 'Running':
			actChar1 = self.lineEdit_ActivityChar1.text()
			actChar2 = self.lineEdit_ActivityChar2.text()
			distUnits = 'km'
		elif activityType == 'Push Ups':
			print('This activity still needs to be fully implemented.')
			return
			actChar1 = self.lineEdit_ActivityChar1.text()
			actChar2 = self.lineEdit_ActivityChar2.text()
			distUnits = ''
		elif activityType == 'Sit Ups':
			print('This activity still needs to be fully implemented.')
			return
			actChar1 = self.lineEdit_ActivityChar1.text()
			actChar2 = self.lineEdit_ActivityChar2.text()
			distUnits = ''
		elif activityType == 'Meditating':
			actChar1 = self.lineEdit_ActivityChar1.text()
			actChar2 = '""'
			distUnits = ''
		else:
			print('Combo box Activity not recognized: {}'.format(activityType))

		#print(activityType)
		#print(activityDate)
		#print(actChar1)
		#print(actChar2)
		
		# Opening Connection to database and inserting row
		conn = sqlite3.connect(self.aux_db_path) #"ElanDB.sqlite")
		c = conn.cursor()

		com = "INSERT INTO Activities (activity, duration, distance, distanceUnits, activityDate, entryDate) VALUES"
		val_list = "('{}',{},{},'{}',{},{})".format(activityType, actChar1, actChar2, distUnits, activityDate, self.today_int)
		print(com + val_list)
		c.execute(com + val_list)

		# Save (commit) the changes and close connection
		conn.commit()
		conn.close()

		# Updating indices and colors.
		self.update_Index_Colors()

		self.label_diagnostic.setText('Entered activity '+val_list+' into the database.')

	def DrawPlot(self):
		# For examples of various plotting functions run the following from ipython
		#		import pyqtgraph.examples
		#		pyqtgraph.examples.run()

		# Gathering Plotting Characteristics
		var_choice = self.comboBox_PlotVar.currentText()
		trans_choice = self.comboBox_PlotTrans.currentText()
		horizon_choice = self.comboBox_PlotHorizon.currentText()

		# Setting horizon chosen
		if horizon_choice == 'Year To Date':
			horizon = (date.today() - date(2018,1,1)).days
		elif horizon_choice == 'Last 1 Month':
			horizon = 31
		elif horizon_choice == 'Last 6 Months':
			horizon = 183
		elif horizon_choice == 'Last Year':
			horizon = 366
		else:
			print('Horizon choice not recognized: {}'.format(horizon_choice))

		# Setting variable of interest chosen
		if var_choice == 'Running Distance':
			raw_data = pd.DataFrame({'date':self.runDB.ActivityDate, 
										'value': self.runDB.Distance})
		elif var_choice == 'Running Time':
			raw_data = pd.DataFrame({'date':self.runDB.ActivityDate, 
										'value': self.runDB.Duration})
		elif var_choice == 'Running Speed':
			raw_data = pd.DataFrame({'date':self.runDB.ActivityDate, 
										'value': self.runDB.Distance/self.runDB.Duration*60})
		elif var_choice == 'Weight':
			print('Weight graphing not implemented yet')
			raw_data = pd.DataFrame({'date':self.runDB.ActivityDate, 
										'value': self.runDB.Distance})
		elif var_choice == 'Articles Read':
			print('Articles read graphing not implemented yet')
			raw_data = pd.DataFrame({'date':self.runDB.ActivityDate, 
										'value': self.runDB.Distance})
		else:
			print('Variable choice not recognized: {}'.format(var_choice))
		
		first_date = date.today() - timedelta(days=horizon)
		last_date = date.today()
		# Small correction to include more data if running avgs
		if trans_choice in ['Running Daily Avg. (1 wk)', 'Running Weekly Avg. (1 wk)']:
			first_date = first_date - timedelta(days=7)
		if trans_choice in ['Running Daily Avg. (1 mth)', 'Running Weekly Avg. (1 mth)']:
			first_date = first_date - timedelta(days=30)
		
		# Filtering to the current horizon
		mask = ((raw_data['date'] >= first_date) & (raw_data['date'] <= last_date))
		raw_data = raw_data[mask].reset_index(drop = True)

		# Adding in 0 placeholders if first and last dates do not have data
		if first_date != raw_data.date[0]:
			line = pd.DataFrame({'date': first_date, 'value': 0}, index=[0])
			raw_data = pd.concat([line, raw_data]).reset_index(drop = True)
		if last_date != raw_data.date.iloc[-1]:
			raw_data = raw_data.append({'date': last_date, 'value':0}, ignore_index = True)
		#print(raw_data)

		# Setting and Implementing Transformation chosen
		if trans_choice == 'Raw':
			new_data = interpolate(raw_data, 'zeros')
		elif trans_choice == 'Raw (interpolated)':
			new_data = interpolate(raw_data, 'linear')
		elif trans_choice == 'Cumulative':
			print('Still need to implement cumulative transformation.')
			cummed = cumulative_series(raw_data)
			new_data = interpolate(cummed, 'linear')
		elif trans_choice == 'Running Daily Avg. (1 wk)':
			print('Still need to implement running average transformation.')
			new_data = running_average(raw_data, 7)
		elif trans_choice == 'Running Daily Avg. (1 mth)':
			print('Still need to implement running average transformation.')
			new_data = running_average(raw_data, 30)
		elif trans_choice == 'Running Weekly Avg. (1 wk)':
			print('Still need to implement running average transformation.')
			new_data = running_average(raw_data, 7)
			new_data.value = new_data.value*7
		elif trans_choice == 'Running Weekly Avg. (1 mth)':
			print('Still need to implement running average transformation.')
			new_data = running_average(raw_data, 30)
			new_data.value = new_data.value*7
		else:
			print('Transformation choice not recognized: {}'.format(trans_choice))

		#print(new_data)

		# Clearing the legend if there is one (this will throw an exception the first time)
		try:
			self.dataplot_legend.scene().removeItem(self.dataplot_legend)
			#self.dataplot.removeItem(self.dataplot_legend)
		except Exception as e:
			print(e)

		# Plotting the final transformed data sets
		dates_converted = [time.mktime(item.timetuple()) for item in new_data.date]
		self.dataplot.clear()
		self.dataplot_legend = self.dataplot.addLegend()
		self.dataplot.plot(dates_converted, new_data.value, pen=(0,255,0), name = var_choice)

		# Plotting Goal Lines
		if var_choice == 'Running Distance' and trans_choice == 'Cumulative':
			goal_dates = pd.date_range(first_date, last_date)
			goal_dates = [time.mktime(item.timetuple()) for item in goal_dates]
			num_days = len(goal_dates)
			total_dist_goal = np.linspace(0, 1000*num_days/365, num_days)
			self.dataplot.plot(goal_dates, total_dist_goal, pen=(255,0,0), name = 'Goal')
		elif var_choice == 'Running Distance' and trans_choice == 'Running Weekly Avg. (1 wk)':
			goal_dates = pd.date_range(first_date, last_date)
			goal_dates = [time.mktime(item.timetuple()) for item in goal_dates]
			num_days = len(goal_dates)
			avg_dist_goal = [self.index_Goals[1]]*num_days
			self.dataplot.plot(goal_dates, avg_dist_goal, pen=(255,0,0), name = 'Goal')


	#################################### Auxiliary Methods (Misc.) ##################################
	def initConfig(self):
		print('reading config file')
		config = configparser.ConfigParser()
		config.read("config.ini")

		# Grabbing the variable values as specified in the config file
		self.mend_db_path = config.get("Data Sources", "mendeley_DB_path")  #"Data Sources" refers to the section in config.ini that holds the vars
		print("Read value: "+self.mend_db_path)
		self.aux_db_path = config.get("Data Sources", "aux_DB_path")
		print("Read value: "+self.aux_db_path)
		self.index_Goals = [ 0, 0, 0 ]
		self.index_Goals[0] = int(config.get("Indices", "index_goal_mind"))
		self.index_Goals[1] = int(config.get("Indices", "index_goal_body"))
		self.index_Goals[2] = int(config.get("Indices", "index_goal_soul"))

	def get_article_db(self):
		#dbpath = "C:\\Users\\Phoenix\\Documents\\Programming\\ArticleDashboard"
		#os.chdir(dbpath)
		conn = sqlite3.connect(self.mend_db_path)  #'MendCopy2.sqlite')
		c = conn.cursor()

		# Selects the first author from each document
		firstAuthors = "SELECT firstNames, min(lastName) as lastName, documentID FROM documentContributors "
		firstAuthors += "GROUP BY documentID ORDER BY documentID"
		# Selects all the documents and joins with first author
		command = "SELECT d.id, c.lastName, c.firstNames, d.title, d.year, d.read, d.added, d.modified, f.localUrl "
		command += "FROM documents AS d JOIN "
		command += "("+firstAuthors+")"
		command += "AS c ON d.ID=c.documentID "  #DocumentContributors AS c ON d.ID="
		command += "JOIN DocumentFiles AS df ON d.ID=df.documentId "
		command += "JOIN Files as f ON f.hash=df.hash"
		command += " WHERE d.deletionPending='false'"
		#print(command)
		c.execute(command)

		doc = c.fetchall()
		cols = ['ID', 'LName', 'FName', 'Title', 'Year', 'MendRead', 'MendDateAdd', 'MendDateMod', 'Path']
		df = pd.DataFrame(doc, columns=cols)
		df['MendDateAdd'] = pd.to_datetime(df['MendDateAdd'], unit='ms').dt.date
		df['MendDateMod'] = pd.to_datetime(df['MendDateMod'], unit='ms').dt.date

		df['Author1'] = df['LName']+", "+df['FName']

		# Flagging all records of downloaded files
		# TODO: Flag the files in appdata folder (since they will likely cause issues)

		# This will try to retrieve the file creation and modification dates usin the local url
		# TODO Catch errors when the files are not found
		url_to_path = lambda x:urllib.request.unquote(urllib.request.unquote(x[8:]))
		df['Path'] = df['Path'].apply(url_to_path)
		df['DateCreated'] = df['Path'].apply(lambda x: date.fromtimestamp(os.path.getctime(x)) if os.path.exists(x) else self.null_date)
		df['DateModifiedF'] = df['Path'].apply(lambda x: date.fromtimestamp(os.path.getmtime(x)) if os.path.exists(x) else self.null_date)

		# Add in a column that gives the date the file was read (taken from the local DB)
		#df['DateRead'] = null_date #date.today()  #None
		elanConn = sqlite3.connect(self.aux_db_path) #"ElanDB.sqlite")
		elanC = elanConn.cursor()
		elanC.execute("SELECT Doc_ID, DateRead FROM ArticlesRead")
		elanDB = pd.DataFrame(elanC.fetchall(), columns=['Doc_ID', 'DateRead'])

		# Left merging in any documents in my DB marked as read
		df2 = pd.merge(df, elanDB, how='left', left_on='ID', right_on='Doc_ID')
		df2['DateRead'].fillna(value = self.null_date_int, inplace=True)
		df2['DateRead']= df2['DateRead'].apply(lambda x: date(int(str(x)[0:4]), int(str(x)[4:6]), int(str(x)[6:8])))

		# read = (df['DateCreated'] != df['DateModifiedF']) & (df['DateModifiedF'] < date.today() - timedelta(days=90))
		# df.ix[read, 'DateRead'] = df['DateModifiedF']

		df2['Author2'] = ''  # Place holder for later addition of a second author

		#### Extracting Folders and Folder Assignments to Add to Doc List
		c.execute("SELECT id , name, parentId FROM Folders")
		self.folders = pd.DataFrame(c.fetchall(), columns=['Folder_ID', 'Name', 'Parent_ID'])

		c.execute("SELECT documentID, folderId FROM DocumentFoldersBase") # WHERE status= 'ObjectUnchanged'")
		self.doc_folders = pd.DataFrame(c.fetchall(), columns = ['ID', 'Folder_ID'])
		self.doc_folders = self.doc_folders.merge(self.folders, how = 'left', on = 'Folder_ID')
		#print(self.doc_folders)

		# Adding labels for the parent folders (TODO?)

		# Concatenating all the Projects for each file
		proj_names = self.doc_folders.groupby('ID')['Name'].apply(lambda x: ', '.join(x)).to_frame('Projects').reset_index()

		# Merging Into Doc DataFrame
		df2 = df2.merge(proj_names, how='left', on ='ID')
		df2['Projects'].fillna(value = '', inplace=True)

		# Reordering columns (for how they will be displayed) and dropping a few unused ones (FName, LName, DocID)
		df2 = df2[['ID', 'Author1', 'Author2', 'Year', 'Title', 'DateRead', 'DateCreated', 'DateModifiedF', 
					'Path', 'MendDateAdd', 'MendDateMod', 'MendRead', 'Projects']]

		conn.close()
		elanConn.close()
		return df2

def main():
	app = QtWidgets.QApplication(sys.argv)
	window = QtWidgets.QMainWindow()
	form = theDashboardApp()
	form.show()
	app.exec_()

if __name__ == '__main__':
	main()
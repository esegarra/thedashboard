from PyQt5.QtCore import (Qt, QStringListModel)
from PyQt5.QtWidgets import (QApplication, QWidget, QTableView, QHBoxLayout)


class MainWindow(QWidget):

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.table_view_1 = QTableView()
        self.table_view_2 = QTableView()
        self.table_view_3 = QTableView()
        items = ['apples', 'bananas', 'cookies', 'drinks', 'eggs', 'flour', 'gatorade']
        items_2 = ['alligator', 'bear', 'cat', 'deer', 'elephant', 'flamingo', 'goose']
        items_3 = ['Arsenic', 'Boron', 'Carbon', 'Dysprosium', 'Europium', 'Flourine', 'Gold']
        self.model = QStringListModel(items)
        self.model_2 = QStringListModel(items_2)
        self.model_3 = QStringListModel(items_3)

        self.table_view_1.setModel(self.model)
        self.table_view_2.setModel(self.model)
        self.table_view_3.setModel(self.model_3)
        self.layout = QHBoxLayout()

        self.list_of_tables = [self.table_view_1,self.table_view_2, self.table_view_3]

        def move_other_scrollbars(idx,bar):
            scrollbars = {tbl.verticalScrollBar() for tbl in self.list_of_tables}
            scrollbars.remove(bar)
            for bar in scrollbars:
                bar.setValue(idx)

        for tbl in self.list_of_tables:
            scrollbar = tbl.verticalScrollBar()
            scrollbar.valueChanged.connect(lambda idx,bar=scrollbar: move_other_scrollbars(idx, bar))
            self.layout.addWidget(tbl)

        self.setLayout(self.layout)

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
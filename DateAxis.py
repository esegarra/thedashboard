import pyqtgraph as pg
import numpy as np
import time

class DateAxis(pg.AxisItem):
	def tickStrings(self, values, scale, spacing):
		strns = []
		rng = max(values)-min(values)
		#if rng < 120:
		#    return pg.AxisItem.tickStrings(self, values, scale, spacing)
		if rng < 3600*24:							# Range is within a day
			string = '%H:%M:%S'
			label1 = '%b %d -'
			label2 = ' %b %d, %Y'
		elif rng >= 3600*24 and rng < 3600*24*30:	# Range is within a month
			string = '%d'
			label1 = '%b - '
			label2 = '%b, %Y'
		elif rng >= 3600*24*30 and rng < 3600*24*30*24:  # Range is within 2 years
			string = '%b'
			label1 = '%Y -'
			label2 = ' %Y'
		elif rng >=3600*24*30*24:					# Range is larger than 2 years
			string = '%Y'
			label1 = ''
			label2 = ''

		# My Edit (simply overrides the above)
		string = '%m-%d'

		for x in values:
			try:
				#strns.append(str(round(x%10000/100))+'/'+str(x%100))
				#strns.append('fixme')
				strns.append(time.strftime(string, time.localtime(x)))    # This is the original
			except ValueError:  ## Windows can't handle dates before 1970
				strns.append('')
		try:
			#label = time.strftime(label1, time.localtime(min(values)))+time.strftime(label2, time.localtime(max(values)))
			label = ''
		except ValueError:
			label = ''
		#self.setLabel(text=label)
		return strns
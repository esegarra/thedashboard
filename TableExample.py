import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QAction, QTableWidget,QTableWidgetItem,QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
 
class App(QWidget):
 
    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 table - pythonspot.com'
        self.left = 200
        self.top = 200
        self.width = 300
        self.height = 200
        self.initUI()
 
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
 
        self.createTable()
 
        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget) 
        self.setLayout(self.layout) 
 
        # Show widget
        self.show()
 
    def createTable(self):
       # Create table
        self.tableWidget = QTableWidget()
        numRows = 20
        numCols = 10
        self.tableWidget.setRowCount(numRows)
        self.tableWidget.setColumnCount(numCols)
        for r in range(0,numRows):
            for c in range(0,numCols):
                self.tableWidget.setItem(r,c, QTableWidgetItem("Cell ({},{})".format(r+1,c+1)))
        self.tableWidget.move(100,100)
        
        # Changing column headings
        self.tableWidget.setHorizontalHeaderLabels(["a", "b", "c", "d", "e"]) 

        # Removing the row labels
        self.tableWidget.verticalHeader().setVisible(False)

        # Setting custom column widths
        self.tableWidget.setColumnWidth(1,40)

        # table selection change
        self.tableWidget.doubleClicked.connect(self.on_click)
 
    @pyqtSlot()
    def on_click(self):
        print("\n")
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            print(currentQTableWidgetItem.row(), currentQTableWidgetItem.column(), currentQTableWidgetItem.text())
 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
import pandas as pd
from datetime import datetime,  date, timedelta

# Functions for doing various time series transformations

def interpolate(data, interp_type):
	'''
	This takes in a data frame with two columns: 'date' and 'value'
		and returns a similar data frame that has interpolated all values
		between the missing dates (to yield a full time series)
	:param data: Data Frame with two fields (note it is assumed that the dates are sorted)
		'date' field should be a datetime.date object
		'value' field should be numeric
	:param interp_type: Indcaites what tyoe of interpolation to be executed
		among the following possibilities
		'zeros': Simply places zeros in between dates observed
		'linear': Interpolates linear between points
		'plateau first': Constant interpolation using the first value 
		'plateau second': Constant interpolation using the second value
	'''
	# Resetting the index to start at 0
	data.reset_index(inplace = True, drop=True)

	# First it aggregates multiple values on a single day
	print('Still need to implement data aggregatation in interpolation function, ignoring for now.')

	# Creating full tange of dates based off of first and last in data
	date_range = pd.date_range(data['date'].iloc[0], data['date'].iloc[-1])
	# Converting timestamp into datetime.date
	date_range = [item.date() for item in date_range]
	#print(date_range)

	new_values = [0]*len(date_range)
	date_range_index = 0

	# Iterating through the records in data
	for i in range(data.shape[0]-1): # Looking at pairs (so last is auto included)
		curr_Date = data.date[i]
		next_Date = data.date[i+1]
		curr_val = data.value[i]
		next_val = data.value[i+1]
		interp_steps = (next_Date - curr_Date).days
		# Setting current value
		new_values[date_range_index] = curr_val
		date_range_index += 1
		if (interp_steps != 1): # Dates are not adjacent so need to interpolate
			linear_step_size = (next_val - curr_val)/interp_steps
			for j in range(interp_steps-1):
				if interp_type == 'zeros':
					new_values[date_range_index] = 0
				elif interp_type == 'linear':
					new_values[date_range_index] = curr_val + (j+1)*linear_step_size
				elif interp_type == 'plateau first':
					new_values[date_range_index] = curr_val
				elif interp_type == 'plateau second':
					new_values[date_range_index] = next_val
				else:
					print('Interpolation method type not recognized ({}), exiting.'.format('interp_type'))
				date_range_index += 1
		
		# Checking if this is the final loop (in which case set the final value)
		if (i == data.shape[0]-2):
			new_values[date_range_index] = next_val

	new_data = pd.DataFrame({'date':date_range, 'value':new_values})
	return(new_data)

def cumulative_series(data):
	'''
	This function creates the cumulative time series given a dataframe of timeseries data
	'''
	# Resetting the index to start at 0
	data.reset_index(inplace = True, drop=True)

	# First it aggregates multiple values on a single day
	print('Still need to implement data aggregatation in cumulative function, ignoring for now.')

	new_values = [0]*data.shape[0]
	new_values[0] = data.value[0]
	for i in range(data.shape[0]-1):
		new_values[i+1] = new_values[i] + data.value[i+1]

	new_data = pd.DataFrame({'date':data.date, 'value':new_values})
	return(new_data)

def running_average(data, avg_horizon):
	'''
	This function creates the a running average time series given a dataframe of 
	timeseries data. Keep in mind that the series returned will be 'avg_horizon' periods
	shorter than the time series sent.
	:param data:
	:param avg_horizon: The number of days in the past that the running average is calculated over
	'''

	# First the data is interpolated to put zeros in missing dates
	raw_data = interpolate(data, 'zeros')

	avg_values = [0]*(raw_data.shape[0]-avg_horizon+1)

	# Here the running averae is calculated
	for i in range(raw_data.shape[0]-avg_horizon+1):
		#print('{}, {}'.format(i, i+avg_horizon-1))
		avg_values[i] = sum(raw_data.value[i:i+avg_horizon])/avg_horizon

	#print(raw_data.shape)
	#print(avg_horizon)
	#print(len(avg_values))
	new_data = pd.DataFrame({'date':raw_data.date[avg_horizon-1:], 'value':avg_values})
	new_data.reset_index(inplace = True, drop = True)
	return(new_data)


''' This is for testing the above Functions
import pandas as pd
import datetime as dt
from datetime import timedelta
from time_series_functions import *

z = [dt.date(2018,1,27), dt.date(2018,2,1), dt.date(2018,2,6)]
vals = [1,4,2]
data = pd.DataFrame({'date':z, 'value':vals})
print(interpolate(data, 'zeros'))
print(interpolate(data, 'linear'))
print(interpolate(data, 'plateau first'))

print(cumulative_series(data))

first_date = dt.date.today() - timedelta(days=10)
last_date = dt.date.today()
dates = pd.date_range(first_date, last_date)
vals = [1,5,2,9,0,2,4,2,3,3,5]
data = pd.DataFrame({'date':dates, 'value':vals})
print(running_average(data, 4))
'''